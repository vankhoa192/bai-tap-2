<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Bài tập 2</title>
	<link rel="stylesheet" href="css/style.css">
	<script type="text/javascript" src="js/jquery.min.js"></script>
	<script type="text/javascript" src="js/json.js"></script>
</head>
<body>
	<header>
		<a href="#" id="logo">Logo bài tập 2</a>
		<nav>
			<ul>
				<li><a href="#">App</a></li>
				<li><a href="#">Game</a></li>
				<li><a href="#" class="current">Movies</a></li>
				<li><a href="#">Books</a></li>
				<li><a href="#">Newspapers</a></li>
			</ul>
		</nav>
	</header>
	<div id="page-top">
		<div id="content">
			<div class="blueberry">
				<ul class="slides">
					<li><img src="img/s1.jpg" alt=""></li>
				</ul>
			</div>
		</div>
	</div>
	<div class="clear"></div>


	<article><div id="result"></div></article>

	<footer class="second">
		<p>Coppy 2015 by Khoa</p>
	</footer>
</body>
</html>